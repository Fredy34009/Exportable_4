package com.financiero.dao;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.financiero.models.Actividades;
import com.financiero.models.FaseDetalles;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;
import com.financiero.util.HibernateUtil;

@Repository
public class FaseDetallesImp extends AbstractFacade<FaseDetalles> implements Dao<FaseDetalles> {

	Session session = HibernateUtil.factory().openSession();
	
	public FaseDetallesImp() {
		super(FaseDetalles.class);
	}
	
	public List<FaseDetalles> actividadesGerente()
	{
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<FaseDetalles> cq = cb.createQuery(FaseDetalles.class);
		Root<FaseDetalles> act = cq.from(FaseDetalles.class);
		
		Join<FaseDetalles, Actividades> actij=act.join("actividades");
		
		cq.where(cb.equal(actij.get("registro"), 2),cb.and(cb.equal(act.get("fases"), 2)));
		//cq.groupBy(actij.get("registro"));
		
		return session.createQuery(cq).getResultList();
	}
}
