package com.financiero.dao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import org.hibernate.Session;

import com.financiero.models.Actividades;
import com.financiero.models.Usuarios;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;
import com.financiero.util.HibernateUtil;

public class ActividadImp extends AbstractFacade<Actividades> implements Dao<Actividades> {

	Session session = HibernateUtil.factory().openSession();

	public ActividadImp() {
		super(Actividades.class);
	}

	//Metodo que ordena por fecha mas reciente
	public List<Actividades> actividadesOrderAsc() {
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Actividades> cq = cb.createQuery(Actividades.class);
		Root<Actividades> act = cq.from(Actividades.class);

		cq.orderBy(cb.asc(act.get("fecha")));
		return session.createQuery(cq).getResultList();
	}
	//Metodo que busca tarea por usuario
	public List<Actividades> actividadesXUsuario(Usuarios us)
	{
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Actividades> cq = cb.createQuery(Actividades.class);
		Root<Actividades> act = cq.from(Actividades.class);
		System.out.println("id usuario "+us.getId());
		cq.where(cb.equal(act.get("usuarios"), us.getId()),cb.and(cb.equal(act.get("estados"),2)));
		cq.orderBy(cb.asc(act.get("fecha")));
		
		return session.createQuery(cq).getResultList();
	}
	//Metodo que busca por fechas las actividades de ha habilitado el asistente
		public List<Actividades> actividadesXFecha(String inicio,String fin)
		{
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Actividades> cq = cb.createQuery(Actividades.class);
			Root<Actividades> act = cq.from(Actividades.class);
			SimpleDateFormat formato=new SimpleDateFormat("yyyy-MM-dd");
			try {
				cq.where(cb.equal(act.get("estados"), 2),cb.between(act.get("fecha"), formato.parse(inicio), formato.parse(fin)));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return session.createQuery(cq).getResultList();
		}

}
