package com.financiero.dao;

import com.financiero.models.TipoUsuarios;
import com.financiero.util.AbstractFacade;
import com.financiero.util.Dao;

public class TipoUsuarioImp extends AbstractFacade<TipoUsuarios> implements Dao<TipoUsuarios> {

	public TipoUsuarioImp() {
		super(TipoUsuarios.class);
	}

}
