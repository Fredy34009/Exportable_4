package com.financiero.util;

import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.financiero.dao.ActividadImp;
import com.financiero.dao.EstadoImp;
import com.financiero.dao.FaseDetallesImp;
import com.financiero.dao.FasesImp;
import com.financiero.dao.FechasEspecialesImp;
import com.financiero.dao.PersonasImp;
import com.financiero.dao.TipoUsuarioImp;
import com.financiero.dao.UsuarioImp;
import com.financiero.models.Actividades;
import com.financiero.models.Estados;
import com.financiero.models.FaseDetalles;
import com.financiero.models.Fases;
import com.financiero.models.FechasEspeciales;
import com.financiero.models.Personas;
import com.financiero.models.TipoUsuarios;
import com.financiero.models.Usuarios;

@Configuration
@EnableWebMvc
@ComponentScan("com.financiero")
public class Config {
	
	@Bean
	public InternalResourceViewResolver resolver()
	{
		InternalResourceViewResolver rs=new InternalResourceViewResolver();
		rs.setPrefix("/WEB-INF/view/");
		rs.setSuffix(".jsp");
		return rs;
	}
	@Bean
	public SessionFactory conn()
	{
		return HibernateUtil.factory();
	}
	@Bean
	public Dao<Fases> faseImp()
	{
		return new FasesImp();
	}
	@Bean
	public Dao<Estados> estadosImp()
	{
		return new EstadoImp();
	}
	@Bean
	public Dao<Usuarios> usuariosImp()
	{
		return new UsuarioImp();
	}
	@Bean
	public UsuarioImp usuariosImple()
	{
		return new UsuarioImp();
	}
	@Bean
	public Dao<Actividades> actividadImp()
	{
		return new ActividadImp();
	}
	@Bean
	public Dao<TipoUsuarios> tipoUsuarioImp()
	{
		return new TipoUsuarioImp();
	}
	@Bean
	public Dao<Personas> personaImp()
	{
		return new PersonasImp();
	}
	@Bean
	public ActividadImp actividadesImp()
	{
		return new ActividadImp();
	}
	@Bean
	public Dao<FechasEspeciales> feDao()
	{
		return new FechasEspecialesImp();
	}
	@Bean
	public Dao<Estados> estadosDao()
	{
		return new EstadoImp();
	}
	@Bean
	public Dao<FaseDetalles> faseDetalleDao()
	{
		return new FaseDetallesImp();
	}
	@Bean
	public FaseDetallesImp faseDetalles()
	{
		return new FaseDetallesImp();
	}
}
