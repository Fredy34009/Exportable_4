package com.financiero.models;
// Generated 12-12-2019 01:19:34 PM by Hibernate Tools 5.2.12.Final

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * FaseDetalles generated by hbm2java
 */
@Entity
@Table(name = "fase_detalles", catalog = "financiero")
public class FaseDetalles implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private Actividades actividades;
	private Fases fases;
	private Date fechaCambio;

	public FaseDetalles() {
	}

	public FaseDetalles(int id, Actividades actividades, Fases fases, Date fechaCambio) {
		this.id = id;
		this.actividades = actividades;
		this.fases = fases;
		this.fechaCambio = fechaCambio;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "actividad", nullable = false)
	public Actividades getActividades() {
		return this.actividades;
	}

	public void setActividades(Actividades actividades) {
		this.actividades = actividades;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fase", nullable = false)
	public Fases getFases() {
		return this.fases;
	}

	public void setFases(Fases fases) {
		this.fases = fases;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "fecha_cambio", nullable = false, length = 10)
	public Date getFechaCambio() {
		return this.fechaCambio;
	}

	public void setFechaCambio(Date fechaCambio) {
		this.fechaCambio = fechaCambio;
	}

}
