package com.financiero.control;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.financiero.dao.ActividadImp;
import com.financiero.dao.FaseDetallesImp;
import com.financiero.models.Actividades;
import com.financiero.models.Estados;
import com.financiero.models.FaseDetalles;
import com.financiero.models.Fases;
import com.financiero.models.Usuarios;
import com.financiero.util.Dao;

import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class ActividadesManager {

	@Autowired
	Dao<Actividades> actividadImp;
	@Autowired
	Dao<Usuarios> usuariosImp;
	@Autowired
	ActividadImp actividadesImp;
	@Autowired
	Dao<Estados> estadosDao;
	@Autowired
	Dao<Fases> faseImp;
	@Autowired
	Dao<FaseDetalles> faseDetalleDao;
	

	@RequestMapping(value = "/actividad", method = RequestMethod.POST)
	public ModelAndView regActividad(ModelAndView mav, @RequestParam("id") int id, @RequestParam("name") String name,
			@RequestParam("des") String des, @RequestParam("fecha") String fech, @RequestParam("user") int iduser)
			throws ParseException {
		Actividades ac = new Actividades();
		Usuarios us = new Usuarios();
		Estados est = new Estados();
		est.setId(1);
		us.setId(iduser);

		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
		Date fecha = formato.parse(fech);

		ac.setNombre(name);
		ac.setDescripcion(des);
		ac.setFecha(fecha);
		ac.setEstados(est);
		ac.setUsuarios(us);

		if (id == 0) {

			actividadImp.create(ac);
			mav.addObject("user", usuariosImp.read());
			mav.addObject("actividad", actividadImp.read());
			mav.addObject("msg", "Se registro la actividad");
		}

		else if (id >= 1) {

			Date fechaActual = new Date();

			// Si la fecha que viene es mayor a la actual actualiza
			if (fecha.after(fechaActual)) {
				ac.setId(id);
				actividadImp.edit(ac);

			}
			// Devuelve un error si la fecha es menor a la ya registrada
			else if (fecha.before(fechaActual)) {
				mav.addObject("msg", "Error la fecha ingresada es anterior");
			}
		}
		mav.addObject("activid", null);
		return regActividad(mav);
	}

	// Metodo que carga la vista principal de actividades
	@RequestMapping(value = "/actividad", method = RequestMethod.GET)
	public ModelAndView regActividad(ModelAndView mav) {
		mav.addObject("user", usuariosImp.read());
		mav.addObject("actividad", actividadesImp.actividadesOrderAsc());
		// mav.addObject("actividad", actividadesImp.actividadesOrderAsc());
		mav.addObject("estados", estadosDao.read());
		mav.setViewName("fecha");
		return mav;
	}

	// Metodo para eliminar una actividad
	@RequestMapping(value = "remActividad", method = RequestMethod.GET)
	public ModelAndView removeActividad(ModelAndView mav, @RequestParam("id") int id) {
		Actividades ac = new Actividades();
		ac.setId(id);
		actividadImp.delete(ac);
		mav.addObject("msg", "Se Elimino la actividad");
		return regActividad(mav);
	}

	// Metodo para consultar una actividad
	@RequestMapping(value = "repActividad", method = RequestMethod.GET)
	public ModelAndView xActividad(ModelAndView mav, @RequestParam("id") int id) {
		Actividades ac = new Actividades();
		ac.setId(id);

		Actividades act = actividadImp.readById(id);
		mav.addObject("msg", null);
		mav.addObject("activid", act);
		return regActividad(mav);
	}

	// Metodo que cambia de estado una actividad
	@RequestMapping(value = "estatechange", method = RequestMethod.POST)
	public ModelAndView stateActividad(ModelAndView mav, @RequestParam("act") int id,
			@RequestParam("estate") int state) {
		Actividades ac = new Actividades();
		Estados es = new Estados();

		ac = actividadImp.readById(id);
		es.setId(state);
		ac.setEstados(es);
		actividadImp.edit(ac);
		return regActividad(mav);
	}

	// Metodo que guarda los detalles de una actividad
	@RequestMapping(value = "/stateOperario", method = RequestMethod.POST)
	public ModelAndView actividadDetalle(ModelAndView mav, @RequestParam("fas") int fase, @RequestParam("act") int id) {

		Actividades act = new Actividades();
		act.setId(id);
		Fases fas = new Fases();
		fas.setId(fase);

		System.out.println("Id " + id);
		System.out.println("Fase " + fase);
		Date fecha = new Date();

		FaseDetalles fDetalles = new FaseDetalles();
		fDetalles.setActividades(act);
		fDetalles.setFechaCambio(fecha);
		fDetalles.setFases(fas);

		System.out.println("Id fd" + fDetalles.getId());
		faseDetalleDao.create(fDetalles);
		mav.setViewName("operario");
		return mav;
	}
}
