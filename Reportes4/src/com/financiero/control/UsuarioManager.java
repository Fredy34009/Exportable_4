package com.financiero.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.financiero.models.Estados;
import com.financiero.models.Personas;
import com.financiero.models.TipoUsuarios;
import com.financiero.models.Usuarios;
import com.financiero.util.Dao;

@Controller
public class UsuarioManager {

	@Autowired
	Dao<Estados> estadosImp;
	@Autowired
	Dao<Personas> personaImp;
	@Autowired
	Dao<TipoUsuarios> tipoUsuarioImp;
	@Autowired
	Dao<Usuarios> usuariosImp;
	
	ModelAndView mav = new ModelAndView();

	@RequestMapping("index.html")
	public ModelAndView inicio() {
		mav.setViewName("reportes");
		return mav;
	}
	
	@RequestMapping(value = "/newPersona",method = RequestMethod.GET)
	public ModelAndView persona (ModelAndView mav)
	{
		mav.addObject("tiposUser",tipoUsuarioImp.read());
		mav.addObject("usuarios",usuariosImp.read());
		mav.setViewName("persona");
		return mav;
	}
	@RequestMapping(value = "/newPersona",method = RequestMethod.POST)
	public ModelAndView persona (ModelAndView mav,@RequestParam("name")String nombre,
			@RequestParam("correo")String correo,@RequestParam("tipou")int tipou,@RequestParam("user")String usuario,@RequestParam("pass")String pass	)
	{
		int i=0;
		List<Usuarios> lista=usuariosImp.read();
		for(Usuarios use: lista)
		{
			if(use.getUsuario().equals(usuario))
			{
				i=1;
			}
		}
		if(i==1)
		{
			mav.addObject("st","Error Nombre de usuario ya registrado");
			mav.setViewName("persona");
			i=0;
		}
		else
		{
			Personas per=new Personas();
			per.setNombre(nombre);
			per.setCorreo(correo);
			
			personaImp.create(per);
			
			TipoUsuarios tu=new TipoUsuarios();
			tu.setId(tipou);
		
			Usuarios us=new Usuarios();
			us.setUsuario(usuario);
			us.setPass(pass);
			us.setPersonas(per);
			us.setTipoUsuarios(tu);
			
			usuariosImp.create(us);
			mav.addObject("st","Se ha completado su registro");
			persona(mav);
		}
		return mav;
	}


	
}
