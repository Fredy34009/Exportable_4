package com.financiero.control;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.financiero.dao.ActividadImp;
import com.financiero.dao.FaseDetallesImp;
import com.financiero.models.Actividades;
import com.financiero.models.FaseDetalles;
import com.financiero.models.Usuarios;
import com.financiero.util.Dao;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class ReportesManager {

	@Autowired
	Dao<Usuarios> usuariosImp;
	@Autowired
	ServletContext servletContext;
	@Autowired
	ServletResponse response;

	@Autowired
	FaseDetallesImp faseDetalles;
	
	@Autowired
	ActividadImp actividadesImp;
	
	@RequestMapping(value = "/actividadG", method = RequestMethod.GET)
	public void regActivida(HttpServletRequest request,HttpServletResponse response,ServletOutputStream outputStream) throws JRException, IOException
	{
		List<FaseDetalles> data=faseDetalles.actividadesGerente();
		JRBeanCollectionDataSource ds=new JRBeanCollectionDataSource(data);
		faseDetalles.actividadesGerente();
		
		File reporte=new File(servletContext.getRealPath("reportes/gerente.jasper"));
		byte bytes[]=JasperRunManager.runReportToPdf(reporte.getPath(), null,ds);
		response.setContentType("application/pdf");
		response.setContentLength(bytes.length);
		outputStream=response.getOutputStream();
		outputStream.write(bytes,0,bytes.length);
		outputStream.flush();
		outputStream.close();
	}
	
	@RequestMapping(value = "/actividadFecha", method = RequestMethod.POST)
	public void regActivida(@RequestParam("fechainicio") String fechainicio,@RequestParam("fechafin") String fin,HttpServletRequest request,HttpServletResponse response,ServletOutputStream outputStream) throws JRException, IOException
	{
		List<Actividades> data=actividadesImp.actividadesXFecha(fechainicio, fin);
		JRBeanCollectionDataSource ds=new JRBeanCollectionDataSource(data);
		
		
		System.out.println("Longitud asistente "+data.size());
		File reporte=new File(servletContext.getRealPath("reportes/asistente.jasper"));
		byte bytes[]=JasperRunManager.runReportToPdf(reporte.getPath(), null,ds);
		response.setContentType("application/pdf");
		response.setContentLength(bytes.length);
		outputStream=response.getOutputStream();
		outputStream.write(bytes,0,bytes.length);
		outputStream.flush();
		outputStream.close();
	}
	//Crea el reporte dependiendo del dato pasado
	@RequestMapping(value = "/repor", method = RequestMethod.GET)
	public void repor(@RequestParam("id")int id) throws JRException, IOException {
		
			List<Usuarios> lista=usuariosImp.read();
			if(id==1)
			{
				System.out.println("Lista "+lista.size());
				JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(lista);
				File reportFile = new File(servletContext.getRealPath("/reportes/usuario.jasper"));

				byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, ds);
				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);
				ServletOutputStream ouputStream = response.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);
				ouputStream.flush();
				ouputStream.close();
				
			}
			else
			{
				JRBeanCollectionDataSource ds = new JRBeanCollectionDataSource(usuariosImp.read());
				File reportFile = new File(servletContext.getRealPath("/reportes/usuario.jasper"));

				byte[] bytes = JasperRunManager.runReportToPdf(reportFile.getPath(), null, ds);
				response.setContentType("application/pdf");
				response.setContentLength(bytes.length);
				ServletOutputStream ouputStream = response.getOutputStream();
				ouputStream.write(bytes, 0, bytes.length);
				ouputStream.flush();
				ouputStream.close();
			}
			return;
	}
	@RequestMapping(value = "/reportes",method = RequestMethod.GET)
	public ModelAndView reportes(ModelAndView mav)
	{
		mav=new ModelAndView();
		mav.setViewName("reportes");
		return mav;
	}
}
