package com.financiero.control;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.financiero.dao.ActividadImp;
import com.financiero.dao.UsuarioImp;
import com.financiero.models.Actividades;
import com.financiero.models.Estados;
import com.financiero.models.Fases;
import com.financiero.models.Usuarios;
import com.financiero.util.Dao;

@Controller
public class LoginManager {

	@Autowired
	UsuarioImp usuariosImple;
	@Autowired
	Dao<Actividades> actividadImp;
	@Autowired
	ActividadImp actividadesImp;
	@Autowired
	Dao<Estados> estadosDao;
	@Autowired
	Dao<Fases> faseImp;

	ModelAndView mav = new ModelAndView();

	@RequestMapping(value = "/usuario", method = RequestMethod.POST)
	public ModelAndView user(@RequestParam("user") String user, @RequestParam("pass") String pass,
			HttpServletRequest request) {
		Usuarios us = new Usuarios();
		us.setUsuario(user);
		us.setPass(pass);

		Usuarios login = usuariosImple.login(us);

		if (login == null) {
			mav.addObject("msg", "<i style='color: red;'>Credenciales invalidas</i>");
			mav.setViewName("inicio");
		} else if(login.getTipoUsuarios().getId().equals(4))
		{
			HttpSession session = request.getSession();
			session.setAttribute("userLog", login);
			mav.addObject("user", usuariosImple.read());
			mav.addObject("msg", null);
			mav.addObject("estados", estadosDao.read());
			mav.addObject("actividad", actividadesImp.actividadesXUsuario(login));
			mav.addObject("fases",faseImp.read());
			mav.setViewName("operario");
			return mav;
		}
		else
		{

			HttpSession session = request.getSession();
			session.setAttribute("userLog", login);
			mav.addObject("user", usuariosImple.read());
			mav.addObject("msg", null);
			mav.addObject("estados", estadosDao.read());
			mav.addObject("actividad", actividadImp.read());
			mav.setViewName("fecha");
		}
		return mav;
	}

	@RequestMapping(value = "/close", method = RequestMethod.GET)
	public ModelAndView cerrar(HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.getAttribute("userLog");
		session.removeAttribute("userLog");
		session.invalidate();
		mav.setViewName("redirect:/index.html");
		return mav;
	}
}
