<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Reportes</title>
<link rel="stylesheet"
	href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
	integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
	crossorigin="anonymous">
</head>
<body>
	<div id="wrapper" class="animate">
		<jsp:include page="menu.jsp" />
		<div class="container">
			<div class="row">
				<div class="col-10">
					<div class="card">
						<div class="card-body">
							<h5 class="card-title">Reporte de Usuarios y actividades</h5>
							<table class="table">
								<thead>
									<tr>
										<td></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Tiempo de fase en proceso para actividades asignadas<a
											class="btn btn-success float-right" href="actividadG">En
												proceso</a></td>
									</tr>
									<tr>
										<td><form action="actividadFecha" method="post">
												<label>Fecha de inicio</label> <input type="date"
													name="fechainicio" /> <br /> <label>Fecha de Fin&emsp;</label>
												<input type="date" name="fechafin" /><button
													class="btn btn-info float-right">Actividades</button>
											</form></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div class="col-8">
					<div class="card"></div>
				</div>
			</div>
		</div>
	</div>

	<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
	<script
		src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
</body>
</html>