create database financiero;
use financiero;

create table estados(
id int primary key auto_increment not null,
estado varchar(25) not null
);

create table fases(
id int primary key auto_increment not null,
fase varchar(25) not null
);

create table personas(
id int primary key auto_increment not null,
nombre varchar(50) not null,
correo varchar(100) not null
);

create table tipo_usuarios(
id int primary key auto_increment not null,
tipo varchar(50) not null
);

create table usuarios(
id int primary key auto_increment not null,
usuario varchar(50) not null unique,
pass varchar(50) not null,
persona  int not  null unique,
tipo_usuario int not null,
foreign key(persona) references personas(id),
foreign key(tipo_usuario) references tipo_usuarios(id)
);

create table fechas_especiales(
id int not null primary key auto_increment,
fecha date not null
);

create table actividades(
id int primary key auto_increment not null,
nombre varchar(50) not null,
descripcion varchar(100) ,
fecha date not null,
estado int not null,
usuario int not null,
foreign key (estado) references estados(id),
foreign key (usuario) references usuarios(id)
);

create table fase_detalles(
id int primary key not null auto_increment,
actividad int not null,
fecha_cambio date not null,
fase int not null,

foreign key(actividad) references actividades(id),
foreign key(fase) references fases(id)
);